//2 
db.users.find(
{ $or: [
{ firstName: { $regex: "s", $options: '$i'}}, {lastName: { $regex: "d", $options: '$i'}}
]},
{ _id: {$slice: firstName, lastName}}

)

//3
db.users.find({ $and: [{ department: "HR" }, { age: { $gte: 70 }} ] })
//4
db.users.find({ $and: [{ firstName: {$regex: "e"} }, { age: { $lte: 30 }} ] })
